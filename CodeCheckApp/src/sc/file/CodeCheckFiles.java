/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sc.file;

import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import sc.CodeCheckApp;
import sc.data.CodeCheckData;
import sc.data.zipFile;

/**
 *
 * @author Yi Shi
 */
public class CodeCheckFiles implements AppFileComponent {

    CodeCheckApp app;

    static final String JSON_NAME = "name";
    static final String JSON_RECENT_WORK = "recent work";
    static final String JSON_NAME_STRING = "name string";
    static final String JSON_TEXTAREA1 = "textarea1";
    static final String JSON_TEXTAREA2 = "textarea2";
    static final String JSON_TEXTAREA3 = "textarea3";
    static final String JSON_TEXTAREA4 = "textarea4";
    static final String JSON_TEXTAREA5 = "textarea5";

    public CodeCheckFiles(CodeCheckApp initApp) {
        app = initApp;
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(filePath);

        // CLEAR THE OLD DATA OUT
        CodeCheckData dataManager = (CodeCheckData) data;
        //dataManager.resetData();

        // NOW LOAD ALL THE DATA FROM THE json OBJECT
        JsonObject titleJson = json.getJsonObject(JSON_NAME);
        JsonObject ta1 = json.getJsonObject(JSON_TEXTAREA1);
        JsonObject ta2 = json.getJsonObject(JSON_TEXTAREA2);
        JsonObject ta3 = json.getJsonObject(JSON_TEXTAREA3);
        JsonObject ta4 = json.getJsonObject(JSON_TEXTAREA4);
        JsonObject ta5 = json.getJsonObject(JSON_TEXTAREA5);

        dataManager.setDirectoryName(titleJson.getString(JSON_NAME));
        dataManager.setTextArea1(ta1.getString(JSON_TEXTAREA1));
        dataManager.setTextArea2(ta2.getString(JSON_TEXTAREA2));
        dataManager.setTextArea3(ta3.getString(JSON_TEXTAREA3));
        dataManager.setTextArea4(ta4.getString(JSON_TEXTAREA4));
        dataManager.setTextArea5a(ta5.getString(JSON_TEXTAREA5));

        File file1 = new File(".\\work\\" + titleJson.getString(JSON_NAME) + " folder\\blackboard");
        File file2 = new File(".\\work\\" + titleJson.getString(JSON_NAME) + " folder\\submissions");
        File file3 = new File(".\\work\\" + titleJson.getString(JSON_NAME) + " folder\\projects");
        File file4 = new File(".\\work\\" + titleJson.getString(JSON_NAME) + " folder\\code");

        if (file1.listFiles().length > 0) {
            for (File file : file1.listFiles()) {
                zipFile zipfile = new zipFile(file);
                dataManager.addBlackBoardSubmissions(zipfile);
            }
        }
        if (file2.listFiles().length > 0) {
            for (File file : file2.listFiles()) {
                zipFile zipfile = new zipFile(file);
                dataManager.addStudentSubmissions(zipfile);
                if (file.getName().length() < 20)
                    dataManager.addstudentZIPFiles(zipfile);
            }
        }
        if (file3.listFiles().length > 0) {
            for (File file : file3.listFiles()) {
                zipFile zipfile = new zipFile(file);
                dataManager.addStudentWorkDirectories(zipfile);
            }
        }
        if (file4.listFiles().length > 0) {
            for (File file : file4.listFiles()) {
                zipFile zipfile = new zipFile(file);
                dataManager.addStudentWork(zipfile);
            }
        }
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        // GET THE DATA
        CodeCheckData dataManager = (CodeCheckData) data;

        JsonObject nameJson = Json.createObjectBuilder().add(JSON_NAME, dataManager.getDirectoryName()).build();
        JsonObject TA1 = Json.createObjectBuilder().add(JSON_TEXTAREA1, dataManager.getTextArea1()).build();
        JsonObject TA2 = Json.createObjectBuilder().add(JSON_TEXTAREA2, dataManager.getTextArea2()).build();
        JsonObject TA3 = Json.createObjectBuilder().add(JSON_TEXTAREA3, dataManager.getTextArea3()).build();
        JsonObject TA4 = Json.createObjectBuilder().add(JSON_TEXTAREA4, dataManager.getTextArea4()).build();
        JsonObject TA5 = Json.createObjectBuilder().add(JSON_TEXTAREA5, dataManager.getTextArea5a()).build();

        // THEN PUT IT ALL TOGETHER IN A JsonObject
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_NAME, nameJson)
                .add(JSON_TEXTAREA1, TA1)
                .add(JSON_TEXTAREA2, TA2)
                .add(JSON_TEXTAREA3, TA3)
                .add(JSON_TEXTAREA4, TA4)
                .add(JSON_TEXTAREA5, TA5)
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }

    public void saveRecentWork(List<String> recentWork, String filePath) throws IOException {
        JsonArrayBuilder workJson = Json.createArrayBuilder();

        ArrayList<String> work = CodeCheckData.getWork();
        for (String Work : work) {
            JsonObject recentWorkJson = Json.createObjectBuilder()
                    .add(JSON_NAME_STRING, Work)
                    .build();
            workJson.add(recentWorkJson);
        }

        JsonArray recentworkArray = workJson.build();
        // THEN PUT IT ALL TOGETHER IN A JsonObject
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_RECENT_WORK, recentworkArray)
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }

    public void loadRecentWork() throws IOException {
        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(".\\work\\recentWork");

        // CLEAR THE OLD DATA OUT
        //CodeCheckData dataManager = (CodeCheckData)data;
        //dataManager.resetData();
        // NOW LOAD ALL THE DATA FROM THE json OBJECT
        JsonArray jsonWorkArray = json.getJsonArray(JSON_RECENT_WORK);

        for (int i = 0; i < jsonWorkArray.size(); i++) {
            JsonObject jsonRecentWork = jsonWorkArray.getJsonObject(i);
            String name = jsonRecentWork.getString(JSON_NAME_STRING);
            CodeCheckData.addRecentWork(name);
        }
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
