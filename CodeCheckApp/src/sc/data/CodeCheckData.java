/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sc.data;

import djf.components.AppDataComponent;
import java.io.File;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sc.CodeCheckApp;

/**
 *
 * @author Yi Shi
 */
public class CodeCheckData implements AppDataComponent {

    CodeCheckApp app;
    static ArrayList<String> recentWork = new ArrayList();
    static String directoryName;

    static ObservableList<zipFile> BlackboardSubmissions;
    static ObservableList<zipFile> StudentSubmissions;
    static ObservableList<zipFile> studentZIPFiles;
    static ObservableList<zipFile> studentWorkDirectories;
    static ObservableList<zipFile> studentWork;
    static String textArea1S = "Successfully extracted files:\nnone\n";
    static String textArea1F = "Submissions Errors:\nnone\n";
    static String textArea1 = "";
    static String textArea2S = "Successfully renamed submissions:\nnone\n";
    static String textArea2F = "Rename Errors:\nnone\n";
    static String textArea2 = "";
    static String textArea3S = "Successfully unzipped files:\nnone\n";
    static String textArea3F = "Unzip Errors:\nnone\n";
    static String textArea3 = "";
    static String textArea4S = "Successfully code extraction:\nnone\n";
    static String textArea4F = "Code extraction Errors:\nnone\n";
    static String textArea4 = "";
    static String textArea5 = "Student Plagiarism Check results can be found at\n";
    static String textArea5a = "";

    public CodeCheckData(CodeCheckApp initApp) {
        app = initApp;
        BlackboardSubmissions = FXCollections.observableArrayList();
        StudentSubmissions = FXCollections.observableArrayList();
        studentZIPFiles = FXCollections.observableArrayList();
        studentWorkDirectories = FXCollections.observableArrayList();
        studentWork = FXCollections.observableArrayList();
    }

    public String getTextArea1() {
        return textArea1;
    }

    public void setTextArea1(String x) {
        textArea1 = x;
    }

    public String getTextArea2() {
        return textArea2;
    }

    public void setTextArea2(String x) {
        textArea2 = x;
    }

    public String getTextArea3() {
        return textArea3;
    }

    public void setTextArea3(String x) {
        textArea3 = x;
    }

    public String getTextArea4() {
        return textArea4;
    }

    public void setTextArea4(String x) {
        textArea4 = x;
    }

    public String getTextArea5a() {
        return textArea5a;
    }

    public void setTextArea5a(String x) {
        textArea5a = x;
    }

    public void setTextArea1S(String text) {
        if (textArea1S.contains("none\n")) {
            textArea1S = textArea1S.substring(0, 30);
        }
        textArea1S += text;
    }

    public String getTextArea1S() {
        return textArea1S;
    }

    public void setTextArea1F(String text) {
        if (textArea1F.endsWith("none\n")) {
            textArea1F = textArea1F.substring(0, 19) + "\n";
        }
        textArea1F += text;
    }

    public String getTextArea1F() {
        return textArea1F;
    }

    public void setTextArea2S(String text) {
        if (textArea2S.contains("none\n")) {
            textArea2S = textArea2S.substring(0, 34);
        }
        textArea2S += text;
    }

    public String getTextArea2S() {
        return textArea2S;
    }

    public void setTextArea2F(String text) {
        if (textArea2F.endsWith("none\n")) {
            textArea2F = textArea2F.substring(0, 15);
        }
        textArea2F += text;
    }

    public String getTextArea2F() {
        return textArea2F;
    }

    public void setTextArea3S(String text) {
        if (textArea3S.contains("none\n")) {
            textArea3S = textArea3S.substring(0, 29);
        }
        textArea3S += text;
    }

    public String getTextArea3S() {
        return textArea3S;
    }

    public void setTextArea3F(String text) {
        if (textArea3F.endsWith("none\n")) {
            textArea3F = textArea3F.substring(0, 14);
        }
        textArea3F += text;
    }

    public String getTextArea3F() {
        return textArea3F;
    }

    public void setTextArea4S(String text) {
        if (textArea4S.contains("none\n")) {
            textArea4S = textArea4S.substring(0, 30);
        }
        textArea4S += text;
    }

    public String getTextArea4S() {
        return textArea4S;
    }

    public void setTextArea4F(String text) {
        if (textArea4F.endsWith("none\n")) {
            textArea4F = textArea4F.substring(0, 24);
        }
        textArea4F += text;
    }

    public String getTextArea4F() {
        return textArea4F;
    }

    public void setTextArea5(String text) {
        textArea5 += text;
    }

    public String getTextArea5() {
        return textArea5;
    }

    public ObservableList<zipFile> getBlackboardSubmissions() {
        return BlackboardSubmissions;
    }

    public void addBlackBoardSubmissions(zipFile file) {
        BlackboardSubmissions.add(file);
    }

    public void removeBlackBoardSubmissions(File file) {
        BlackboardSubmissions.remove(file);
        
    }

    public ObservableList<zipFile> getStudentSubmissions() {
        return StudentSubmissions;
    }

    public void addStudentSubmissions(zipFile file) {
        StudentSubmissions.add(file);
    }

    public void removeStudentSubmissions(File file) {
        StudentSubmissions.remove(file);
    }

    public ObservableList<zipFile> getStudentZIPFiles() {
        return studentZIPFiles;
    }

    public void addstudentZIPFiles(zipFile file) {
        studentZIPFiles.add(file);
    }

    public void removestudentZIPFiles(File file) {
        studentZIPFiles.remove(file);
    }

    public ObservableList<zipFile> getStudentWorkDirectories() {
        return studentWorkDirectories;
    }

    public void addStudentWorkDirectories(zipFile file) {
        studentWorkDirectories.add(file);
    }

    public void removeStudentWorkDirectories(File file) {
        studentWorkDirectories.remove(file);
    }

    public ObservableList<zipFile> getStudentWork() {
        return studentWork;
    }

    public void addStudentWork(zipFile file) {
        studentWork.add(file);
    }

    public void removeStudentWork(ObservableList<zipFile> file) {
        for (zipFile files : file) {
            studentWork.remove(files);
        }
    }

    static public void clearRecentWork() {
        recentWork.clear();
    }

    static public void renameRecentWork(String oldName, String newName) {
        recentWork.set(recentWork.indexOf(oldName), newName);
    }

    static public ArrayList<String> getWork() {
        return recentWork;
    }

    static public String getRecentWork(int index) {
        return recentWork.get(index);
    }

    static public void addRecentWork(String work) {
        recentWork.add(work);
    }

    static public void removeWork(String work) {
        recentWork.remove(work);
    }

    static public void pushRecentWork(String work) {
        if (recentWork.contains(work)) {
            recentWork.remove(work);
            recentWork.add(recentWork.size(), work);
        }
    }

    public String getDirectoryName() {
        return directoryName;
    }

    public void setDirectoryName(String name) {
        directoryName = name;
    }

    @Override
    public void resetData() {
        textArea1S = "Successfully extracted files:\nnone\n";
        textArea1F = "Submissions Errors:\nnone\n";
        textArea1 = "";
        textArea2S = "Successfully renamed submissions:\nnone\n";
        textArea2F = "Rename Errors:\nnone\n";
        textArea2 = "";
        textArea3S = "Successfully unzipped files:\nnone\n";
        textArea3F = "Unzip Errors:\nnone\n";
        textArea3 = "";
        textArea4S = "Successfully code extraction:\nnone\n";
        textArea4F = "Code extraction Errors:\nnone\n";
        textArea4 = "";
        textArea5 = "Student Plagiarism Check results can be found at\n";
        textArea5a = "";
        BlackboardSubmissions.clear();
        StudentSubmissions.clear();
        studentZIPFiles.clear();
        studentWorkDirectories.clear();
        studentWork.clear();
    }

}
