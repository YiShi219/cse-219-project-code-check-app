package sc.data;

import net.lingala.zip4j.core.ZipFile;
import java.io.File;
import java.nio.file.Path;

/**
 *
 * @author Yi Shi
 */
public class zipFile {

    File file;
    String name;
    ZipFile zipFile;

    public zipFile(File file) {
        this.file = file;
        name = file.getName();
        try {
            zipFile = new ZipFile(file);
        } catch (Exception o) {
            o.printStackTrace();
        }
    }

    public Path getPath() {
        return file.toPath();
    }

    public String getName() {
        return name;
    }

    public ZipFile getZipFile() {
        return zipFile;
    }

    public File getFile() {
        return file;
    }

    public boolean renameTo(String name) {
        boolean a;
        File newFile = new File(name + ".zip");
        a = file.renameTo(newFile);
        try {
            zipFile = new ZipFile(file);
        } catch (Exception o) {
            o.printStackTrace();
        }
        name = newFile.getName();
        return a;
    }

    public void extractFile(String destPath) {
        try {
            zipFile.extractAll(destPath);
        } catch (Exception o) {
            o.printStackTrace();
        }
    }

}
