/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sc.style;

/**
 *
 * @author Yi Shi
 */
public class CodeCheckStyle {
    public static String CLASS_HW_TABLE = "hw_table";
    public static String CLASS_PROMPT_LABEL = "prompt_label";
    public static String CLASS_EDIT_TEXT_FIELD = "edit_text_field";
    public static String CLASS_EDIT_BUTTON = "edit_button";
    public static String CLASS_PROGRESS_BAR = "edit_progress_bar";
    public static String CLASS_DARK_TEXT = "edit_dark_text";
    public static String CLASS_LIGHT_TEXT = "edit_light_text";
    public static String CLASS_CHECK_BOX = "edit_check_box";
    public static String CLASS_SPACING = "edit_spacing";
    public static String FILE_TYPE_TEXTFIELD = "edit_file_type_textfield";
    public static String WELCOME_PAGE = "welcome_page";
    public static String WELCOME_PAGE1 = "welcome_page1";
}
