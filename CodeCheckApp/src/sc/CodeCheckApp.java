package sc;

import sc.data.CodeCheckData;
import djf.AppTemplate;
import sc.file.CodeCheckFiles;
import java.util.Locale;
import static javafx.application.Application.launch;
import sc.workspace.CodeCheckWelcomePage;
import sc.workspace.CodeCheckWorkSpace;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Yi Shi
 */
public class CodeCheckApp extends AppTemplate{
    /**
     * This hook method must initialize all four components in the
     * proper order ensuring proper dependencies are respected, meaning
     * all proper objects are already constructed when they are needed
     * for use, since some may need others for initialization.
     */
    @Override
    public void buildAppComponentsHook() {
        // CONSTRUCT ALL FOUR COMPONENTS. NOTE THAT FOR THIS APP
        // THE WORKSPACE NEEDS THE DATA COMPONENT TO EXIST ALREADY
        // WHEN IT IS CONSTRUCTED, SO BE CAREFUL OF THE ORDER
        dataComponent = new CodeCheckData(this);
        workspaceComponent = new CodeCheckWorkSpace(this);
        fileComponent = new CodeCheckFiles(this);
        CodeCheckWelcomePage welcome = new CodeCheckWelcomePage(this);
    }
    
    /**
     * This is where program execution begins. Since this is a JavaFX app it
     * will simply call launch, which gets JavaFX rolling, resulting in sending
     * the properly initialized Stage (i.e. window) to the start method inherited
     * from AppTemplate, defined in the Desktop Java Framework.
     */
    public static void main(String[] args) {
	Locale.setDefault(Locale.US);
	launch(args);
    }
}
