package sc.workspace;

import static sc.CodeCheckProp.HOME_BUTTON_TEXT;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import djf.controller.AppFileController;
import static djf.settings.AppPropertyType.APP_TITLE;
import static djf.settings.AppPropertyType.CODE_CHECK_TOOLTIP;
import static djf.settings.AppPropertyType.EXTRACT_CODE_TOOLTIP;
import static djf.settings.AppPropertyType.EXTRACT_TOOLTIP;
import static djf.settings.AppPropertyType.HOME_TOOLTIP;
import static djf.settings.AppPropertyType.NEW_COMPLETED_MESSAGE;
import static djf.settings.AppPropertyType.NEW_COMPLETED_TITLE;
import static djf.settings.AppPropertyType.NEXT_TOOLTIP;
import static djf.settings.AppPropertyType.PREVIOUS_TOOLTIP;
import static djf.settings.AppPropertyType.REFRESH_TOOLTIP;
import static djf.settings.AppPropertyType.REMOVE_TOOLTIP;
import static djf.settings.AppPropertyType.RENAME_TOOLTIP;
import static djf.settings.AppPropertyType.UNZIP_TOOLTIP;
import static djf.settings.AppPropertyType.VIEW_RESULTS_TOOLTIP;
import static djf.settings.AppPropertyType.VIEW_TOOLTIP;
import static djf.ui.AppGUI.CLASS_BORDERED_PANE;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.io.File;
import java.nio.file.Files;
import java.util.Collection;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.util.zip.ZipFile;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;
import properties_manager.PropertiesManager;
import sc.CodeCheckApp;
import static sc.CodeCheckProp.BLACKBOARD_SUBMISSIONS_TEXT;
import static sc.CodeCheckProp.CHECK_PROGRESS_TEXT;
import static sc.CodeCheckProp.CODE_CHECK_BUTTON_TEXT;
import static sc.CodeCheckProp.CODE_PROGRESS_TEXT;
import static sc.CodeCheckProp.EXTRACTION_PROGRESS_TEXT;
import static sc.CodeCheckProp.EXTRACT_BUTTON_TEXT;
import static sc.CodeCheckProp.EXTRACT_CODE_BUTTON_TEXT;
import static sc.CodeCheckProp.NEXT_BUTTON_TEXT;
import static sc.CodeCheckProp.PREVIOUS_BUTTON_TEXT;
import static sc.CodeCheckProp.REFRESH_BUTTON_TEXT;
import static sc.CodeCheckProp.REMOVE_BUTTON_TEXT;
import static sc.CodeCheckProp.RENAME_FILE_BUTTON_TEXT;
import static sc.CodeCheckProp.RENAME_PROGRESS_TEXT;
import static sc.CodeCheckProp.SOURCE_FILE_TYPES_TEXT;
import static sc.CodeCheckProp.STEP1_INSTRUCTION_TEXT;
import static sc.CodeCheckProp.STEP1_TEXT;
import static sc.CodeCheckProp.STEP2_INSTRUCTION_TEXT;
import static sc.CodeCheckProp.STEP2_TEXT;
import static sc.CodeCheckProp.STEP3_INSTRUCTION_TEXT;
import static sc.CodeCheckProp.STEP3_TEXT;
import static sc.CodeCheckProp.STEP4_INSTRUCTION_TEXT;
import static sc.CodeCheckProp.STEP4_TEXT;
import static sc.CodeCheckProp.STEP5_INSTRUCTION_TEXT;
import static sc.CodeCheckProp.STEP5_TEXT;
import static sc.CodeCheckProp.STUDENT_SUBMISSIONS_TEXT;
import static sc.CodeCheckProp.STUDENT_WORK_DIRECTORIES_TEXT;
import static sc.CodeCheckProp.STUDENT_WORK_TEXT;
import static sc.CodeCheckProp.STUDENT_ZIP_FILES_TEXT;
import static sc.CodeCheckProp.UNZIP_BUTTON_TEXT;
import static sc.CodeCheckProp.UNZIP_PROGRESS_TEXT;
import static sc.CodeCheckProp.VIEW_BUTTON_TEXT;
import static sc.CodeCheckProp.VIEW_RESULTS_BUTTON_TEXT;
import sc.data.CodeCheckData;
import sc.data.zipFile;
import static sc.style.CodeCheckStyle.CLASS_CHECK_BOX;
import static sc.style.CodeCheckStyle.CLASS_DARK_TEXT;
import static sc.style.CodeCheckStyle.CLASS_EDIT_BUTTON;
import static sc.style.CodeCheckStyle.CLASS_EDIT_TEXT_FIELD;
import static sc.style.CodeCheckStyle.CLASS_HW_TABLE;
import static sc.style.CodeCheckStyle.CLASS_LIGHT_TEXT;
import static sc.style.CodeCheckStyle.CLASS_SPACING;
import static sc.style.CodeCheckStyle.FILE_TYPE_TEXTFIELD;

/**
 *
 * @author Yi Shi
 */
public class CodeCheckWorkSpace extends AppWorkspaceComponent {

    CodeCheckApp app;
    CodeCheckController controller;
    CodeCheckData data = new CodeCheckData(app);
    AppFileController fileController;
    int page = 1;
    boolean renamed = false;

    HBox pageToolBar;
    Button homeButton;
    Button previousButton;
    Button nextButton;

    HBox editTableToolBar;
    Button removeButton;
    Button refreshButton;
    Button viewButton;

    ScrollPane hwTableScrollPane;
    TableView<zipFile> hwTableView;
    TableColumn<zipFile, String> studentSubmissionsColumn;
    TableColumn<zipFile, String> blackboardSubmissionsColumn;
    TableColumn<zipFile, String> studentZIPFilesColumn;
    TableColumn<zipFile, String> studentWorkDirectoriesColumn;
    TableColumn<zipFile, String> studentWorkColumn;

    VBox leftBox;
    VBox rightBox;

    Label stepLabel;
    Label stepInstructionLabel;
    HBox progressBox;
    Label progressLabel;
    ProgressBar progressBar;
    ProgressIndicator progressIndicator;
    HBox actionButtonBox;
    Button actionButton;
    Button actionButton2;
    ScrollPane textFieldScrollPane;
    TextArea infoTextArea;
    Label sourceFileTypeLabel;
    CheckBox javaCheckBox;
    CheckBox jsCheckBox;
    CheckBox jppCheckBox;
    CheckBox csCheckBox;
    CheckBox promptCheckBox;
    GridPane fileTypeGridPane;
    TextField fileTypeTextField;
    HBox fileTypeBox;
    Stage welcomeStage;
    Scene welcomeScene;
    BorderPane welcomePane;
    VBox recentWorkBox;
    VBox codeCheckBox;
    Label recentWorkLabel;
    Hyperlink website;

    public CodeCheckWorkSpace(CodeCheckApp initApp) {
        app = initApp;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        fileController = new AppFileController(app);

        // LAYOUT THE APP
        initLayout();

        // HOOK UP THE CONTROLLERS
        initControllers();

        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();

    }

    private void initLayout() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        pageToolBar = new HBox();
        homeButton = new Button(props.getProperty(HOME_BUTTON_TEXT));
        previousButton = new Button(props.getProperty(PREVIOUS_BUTTON_TEXT));
        nextButton = new Button(props.getProperty(NEXT_BUTTON_TEXT));

        editTableToolBar = new HBox();
        removeButton = new Button(props.getProperty(REMOVE_BUTTON_TEXT));
        refreshButton = new Button(props.getProperty(REFRESH_BUTTON_TEXT));
        viewButton = new Button(props.getProperty(VIEW_BUTTON_TEXT));

        hwTableScrollPane = new ScrollPane();
        hwTableView = new TableView();
        studentSubmissionsColumn = new TableColumn(props.getProperty(STUDENT_SUBMISSIONS_TEXT));
        blackboardSubmissionsColumn = new TableColumn(props.getProperty(BLACKBOARD_SUBMISSIONS_TEXT));
        studentZIPFilesColumn = new TableColumn(props.getProperty(STUDENT_ZIP_FILES_TEXT));
        studentWorkDirectoriesColumn = new TableColumn(props.getProperty(STUDENT_WORK_DIRECTORIES_TEXT));
        studentWorkColumn = new TableColumn(props.getProperty(STUDENT_WORK_TEXT));
        blackboardSubmissionsColumn.prefWidthProperty().bind(hwTableView.widthProperty().divide(1));

        leftBox = new VBox();
        rightBox = new VBox();

        stepLabel = new Label(props.getProperty(STEP1_TEXT));
        stepInstructionLabel = new Label(props.getProperty(STEP1_INSTRUCTION_TEXT));
        progressBox = new HBox();
        progressLabel = new Label(props.getProperty(EXTRACTION_PROGRESS_TEXT));
        progressBar = new ProgressBar(0);
        progressBar.setMinWidth(170);
        progressBar.setMinHeight(30);
        progressIndicator = new ProgressIndicator(0);
        actionButtonBox = new HBox();
        actionButton = new Button(props.getProperty(EXTRACT_BUTTON_TEXT));
        textFieldScrollPane = new ScrollPane();
        infoTextArea = new TextArea();

        sourceFileTypeLabel = new Label(props.getProperty(SOURCE_FILE_TYPES_TEXT));
        String java = ".java";
        String js = ".js";
        String cpp = ".c, .h, .cpp";
        String cs = ".cs";

        javaCheckBox = new CheckBox(java);
        jsCheckBox = new CheckBox(js);
        jppCheckBox = new CheckBox(cpp);
        csCheckBox = new CheckBox(cs);
        promptCheckBox = new CheckBox();
        fileTypeBox = new HBox();
        fileTypeTextField = new TextField();
        GridPane fileTypeGridPane = new GridPane();
        website = new Hyperlink("http://www.google.com");

        hwTableView.getColumns().add(blackboardSubmissionsColumn);
        blackboardSubmissionsColumn.setCellValueFactory(new PropertyValueFactory<zipFile, String>("name"));

        // HOOK UP THE TABLE TO THE DATA
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        ObservableList<zipFile> model = data.getBlackboardSubmissions();
        hwTableView.setItems(model);
        hwTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        homeButton.setTooltip(new Tooltip(props.getProperty(HOME_TOOLTIP)));
        nextButton.setTooltip(new Tooltip(props.getProperty(NEXT_TOOLTIP)));
        previousButton.setTooltip(new Tooltip(props.getProperty(PREVIOUS_TOOLTIP)));
        removeButton.setTooltip(new Tooltip(props.getProperty(REMOVE_TOOLTIP)));
        refreshButton.setTooltip(new Tooltip(props.getProperty(REFRESH_TOOLTIP)));
        viewButton.setTooltip(new Tooltip(props.getProperty(VIEW_TOOLTIP)));
        actionButton.setTooltip(new Tooltip(props.getProperty(EXTRACT_TOOLTIP)));

        fileTypeBox.getChildren().add(promptCheckBox);
        fileTypeBox.getChildren().add(fileTypeTextField);

        pageToolBar.getChildren().add(homeButton);
        pageToolBar.getChildren().add(previousButton);
        pageToolBar.getChildren().add(nextButton);
        editTableToolBar.getChildren().add(removeButton);
        editTableToolBar.getChildren().add(refreshButton);
        editTableToolBar.getChildren().add(viewButton);
        hwTableScrollPane.setContent(hwTableView);
        progressBox.getChildren().add(progressLabel);
        progressBox.getChildren().add(progressBar);
        progressBox.getChildren().add(progressIndicator);
        actionButtonBox.getChildren().add(actionButton);
        textFieldScrollPane.setContent(infoTextArea);
        hwTableScrollPane.setFitToWidth(true);
        hwTableScrollPane.setFitToHeight(true);

        app.getGUI().getTopToolbarPane().getChildren().add(pageToolBar);
        BorderPane workspaceBorderPane = new BorderPane();
        leftBox.getChildren().add(stepLabel);
        leftBox.getChildren().add(stepInstructionLabel);
        leftBox.getChildren().add(hwTableView);
        leftBox.getChildren().add(editTableToolBar);
        rightBox.getChildren().add(progressBox);
        rightBox.getChildren().add(actionButtonBox);
        rightBox.getChildren().add(infoTextArea);
        workspaceBorderPane.setCenter(leftBox);
        workspaceBorderPane.setRight(rightBox);

        // AND SET THIS AS THE WORKSPACE PANE
        workspace = workspaceBorderPane;

        //CodeCheckData data = new CodeCheckData(app);
        infoTextArea.setText(data.getTextArea1());
        infoTextArea.setEditable(false);
        homeButton.setDisable(true);
        previousButton.setDisable(true);
        if (data.getStudentSubmissions().size() == 0) {
            nextButton.setDisable(true);
        }
        actionButton.setDisable(true);
        removeButton.setDisable(true);
        viewButton.setDisable(true);
        refreshButton.setDisable(true);
        fileTypeTextField.setDisable(true);
    }

    public void updateWorkSpaceButton() {
        refreshButton.setDisable(false);
        if (data.getStudentSubmissions().size() > 0) {
            nextButton.setDisable(false);
        }
    }

    private void initControllers() {
        controller = new CodeCheckController(app);

        nextButton.setOnAction(e -> {
            page++;
            previousButton.setDisable(false);
            homeButton.setDisable(false);

            removeButton.setDisable(true);
            viewButton.setDisable(true);
            actionButton.setDisable(true);

            updatePage();

            hwTableView.refresh();
            nextButton.setDisable(true);
            if (page == 2 && !data.getStudentZIPFiles().isEmpty()) {
                nextButton.setDisable(false);
                //System.out.println("1");
            } else if (page == 3 && !data.getStudentWorkDirectories().isEmpty()) {
                nextButton.setDisable(false);
                //System.out.println("2");
            } else if (page == 4 && !data.getStudentWork().isEmpty()) {
                nextButton.setDisable(false);
                //System.out.println("3");
            } else if (page == 5) {
                nextButton.setDisable(true);
            }

        });

        previousButton.setOnAction(e -> {
            if (page == 2) {
                previousButton.setDisable(true);
                homeButton.setDisable(true);
            } else {
                previousButton.setDisable(false);
            }
            nextButton.setDisable(false);
            removeButton.setDisable(true);
            viewButton.setDisable(true);
            page--;
            updatePage();
            actionButton.setDisable(true);
            if (page == 2) {
                actionButton.setDisable(false);
            }

        });

        homeButton.setOnAction(e -> {
            page = 1;
            previousButton.setDisable(true);
            nextButton.setDisable(false);
            homeButton.setDisable(true);
            removeButton.setDisable(true);
            viewButton.setDisable(true);
            updatePage();
        });

        actionButton.setOnAction(e -> {

//***************************************************************************************************
            ObservableList<zipFile> file = hwTableView.getSelectionModel().getSelectedItems();
            for (zipFile files : file) {
                if (files.getName().endsWith(".zip")) {
                    files.extractFile(".\\work\\" + data.getDirectoryName() + " folder\\submissions");
                    ZipFile File = null;
                    try {
                        File = new ZipFile(files.getFile());
                    } catch (Exception o) {
                        o.printStackTrace();
                    }
                    Enumeration zipEntries = File.entries();
                    while (zipEntries.hasMoreElements()) {
                        ZipEntry elem = (ZipEntry) zipEntries.nextElement();
                        if (elem.getName().endsWith(".zip")) {
                            data.setTextArea1S(elem.getName() + "\n");
                        }
                    }
                } else {
                    data.setTextArea1F(files.getName() + "\n");
                }
            }

            Task<Void> task = new Task<Void>() {
                double perc = 0;
                double max = 200;

                @Override
                protected Void call() throws Exception {
                    for (int i = 0; i < 200; i++) {
                        perc = i / max;
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                progressIndicator.setProgress(perc);
                                progressBar.setProgress(perc);
                            }

                        });
                        Thread.sleep(5);
                    }
                    data.setTextArea1(data.getTextArea1S() + "\n" + data.getTextArea1F());
                    infoTextArea.setText(data.getTextArea1());
                    return null;
                }
            };
            Thread thread = new Thread(task);
            thread.start();

            File submissions = new File(".\\work\\" + data.getDirectoryName() + " folder\\submissions");
            if (submissions.listFiles().length > 0) {
                nextButton.setDisable(false);
                app.getGUI().getFileController().Saved(false);
            }
            clearSelection();
        });

        refreshButton.setOnAction(e -> {
            File blackboardSubmissions = new File(".\\work\\" + data.getDirectoryName() + " folder\\blackboard");
            boolean contains = false;
            if (blackboardSubmissions.list().length > 0) {
                for (File file : blackboardSubmissions.listFiles()) {
                    zipFile zipFile = null;
                    try {
                        zipFile = new zipFile(file);
                    } catch (Exception o) {
                    }
                    for (int i = 0; i < data.getBlackboardSubmissions().size(); i++) {
                        if (file.getName().equals(data.getBlackboardSubmissions().get(i).getName())) {
                            contains = true;
                        }
                    }
                    if (!contains) {
                        data.addBlackBoardSubmissions(zipFile);
                    }
                    contains = false;
                }
            }
            ObservableList<zipFile> model = data.getBlackboardSubmissions();
            hwTableView.setItems(model);
        });

        removeButton.setOnAction(e -> {
            AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
            yesNoDialog.show("Remove", "Are you sure you want to remove?");
            String selection = yesNoDialog.getSelection();
            if (selection.equals(AppYesNoCancelDialogSingleton.YES)) {
                zipFile file = hwTableView.getSelectionModel().getSelectedItems().get(0);
                try {
                    Files.deleteIfExists(file.getPath());
                    data.getBlackboardSubmissions().remove(file);
                } catch (Exception o) {
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("INFORMATION");
                    alert.setHeaderText("Deletion Failure");
                    alert.setContentText("File named " + file.getName() + " cannot be deleted because it is being used by another process, please try again.");
                    alert.showAndWait();
                }
                removeButton.setDisable(true);
                hwTableView.refresh();
                clearSelection();
                app.getGUI().getFileController().Saved(false);
            }
        });

        hwTableView.setOnMouseClicked(e -> {
            if (hwTableView.getSelectionModel().getSelectedItems().size() != 0) {
                removeButton.setDisable(false);
                actionButton.setDisable(false);
                if (hwTableView.getSelectionModel().getSelectedItems().size() > 1) {
                    viewButton.setDisable(true);
                } else {
                    viewButton.setDisable(false);
                }
            }
        });

        viewButton.setOnAction(e -> {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Directory Contents");
            alert.setHeaderText(null);
            String fileContents = "";
            ZipFile File = null;
            try {
                File = new ZipFile(hwTableView.getSelectionModel().getSelectedItems().get(0).getFile());
            } catch (Exception o) {
            }
            if (File == null) {
                viewButton.setDisable(true);
            } else {
                Enumeration zipEntries = File.entries();
                while (zipEntries.hasMoreElements()) {
                    fileContents += ((ZipEntry) zipEntries.nextElement()).getName() + "\n";
                }
                alert.setContentText(fileContents);
                alert.getDialogPane().setMinWidth(600);
                alert.showAndWait();
            }
        });
    }

    private void initStyle() {
        pageToolBar.getStyleClass().add(CLASS_BORDERED_PANE);
        homeButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        previousButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        nextButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        editTableToolBar.getStyleClass().add(CLASS_SPACING);
        leftBox.getStyleClass().add(CLASS_SPACING);
        rightBox.getStyleClass().add(CLASS_SPACING);

        hwTableView.getStyleClass().add(CLASS_HW_TABLE);

        stepLabel.getStyleClass().add(CLASS_DARK_TEXT);
        stepInstructionLabel.getStyleClass().add(CLASS_LIGHT_TEXT);
        progressLabel.getStyleClass().add(CLASS_DARK_TEXT);
        //progressBar.getStyleClass().add(CLASS_PROGRESS_BAR);
        actionButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        removeButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        refreshButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        viewButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        infoTextArea.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        progressBox.getStyleClass().add(CLASS_SPACING);

    }

    private void updatePage() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (page == 1) {
            infoTextArea.setText(data.getTextArea1());
            if (hwTableView.getColumns().size() == 1) {
                hwTableView.getColumns().remove(0);
                hwTableView.getColumns().add(blackboardSubmissionsColumn);
                blackboardSubmissionsColumn.prefWidthProperty().bind(hwTableView.widthProperty().divide(1));
                blackboardSubmissionsColumn.setCellValueFactory(new PropertyValueFactory<zipFile, String>("name"));
                ObservableList<zipFile> model = data.getBlackboardSubmissions();
                hwTableView.setItems(model);
            }
            progressBar.setProgress(0);
            progressIndicator.setProgress(0);
            leftBox.getChildren().remove(stepLabel);
            stepLabel = new Label(props.getProperty(STEP1_TEXT));
            leftBox.getChildren().add(0, stepLabel);
            stepLabel.getStyleClass().add(CLASS_DARK_TEXT);

            leftBox.getChildren().remove(stepInstructionLabel);
            stepInstructionLabel = new Label(props.getProperty(STEP1_INSTRUCTION_TEXT));
            leftBox.getChildren().add(1, stepInstructionLabel);
            stepInstructionLabel.getStyleClass().add(CLASS_LIGHT_TEXT);

            progressBox.getChildren().remove(progressLabel);
            progressLabel = new Label(props.getProperty(EXTRACTION_PROGRESS_TEXT));
            progressBox.getChildren().add(0, progressLabel);
            progressLabel.getStyleClass().add(CLASS_DARK_TEXT);

            actionButtonBox.getChildren().remove(actionButton);
            actionButton = new Button(props.getProperty(EXTRACT_BUTTON_TEXT));
            actionButtonBox.getChildren().add(0, actionButton);
            actionButton.setTooltip(new Tooltip(props.getProperty(EXTRACT_TOOLTIP)));
            actionButton.getStyleClass().add(CLASS_EDIT_BUTTON);
            actionButton.setDisable(true);
            actionButton.setOnAction(e -> {

                ObservableList<zipFile> file = hwTableView.getSelectionModel().getSelectedItems();
                for (zipFile files : file) {
                    if (files.getName().endsWith(".zip")) {
                        files.extractFile(".\\work\\" + data.getDirectoryName() + " folder\\submissions");
                        ZipFile File = null;
                        try {
                            File = new ZipFile(files.getFile());
                        } catch (Exception o) {
                            o.printStackTrace();
                        }
                        Enumeration zipEntries = File.entries();
                        while (zipEntries.hasMoreElements()) {
                            ZipEntry elem = (ZipEntry) zipEntries.nextElement();
                            if (elem.getName().endsWith(".zip")) {
                                data.setTextArea1S(elem.getName() + "\n");
                            }
                        }
                    } else {
                        data.setTextArea1F(files.getName() + "\n");
                    }
                }

                Task<Void> task = new Task<Void>() {
                    double perc = 0;
                    double max = 200;

                    @Override
                    protected Void call() throws Exception {
                        for (int i = 0; i < 200; i++) {
                            perc = i / max;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    progressIndicator.setProgress(perc);
                                    progressBar.setProgress(perc);
                                }

                            });
                            Thread.sleep(5);
                        }
                        data.setTextArea1(data.getTextArea1S() + "\n" + data.getTextArea1F());
                        infoTextArea.setText(data.getTextArea1());
                        return null;
                    }
                };
                Thread thread = new Thread(task);
                thread.start();

                File submissions = new File(".\\work\\" + data.getDirectoryName() + " folder\\submissions");
                if (submissions.listFiles().length > 0) {
                    nextButton.setDisable(false);
                    app.getGUI().getFileController().Saved(false);
                }
                clearSelection();
                renamed = false;
            });
            refreshButton.setOnAction(e -> {
                File blackboardSubmissions = new File(".\\work\\" + data.getDirectoryName() + " folder\\blackboard");

                boolean contains = false;
                if (blackboardSubmissions.list().length > 0) {
                    for (File file : blackboardSubmissions.listFiles()) {
                        zipFile zipFile = null;
                        try {
                            zipFile = new zipFile(file);
                        } catch (Exception o) {
                        }
                        for (int i = 0; i < data.getBlackboardSubmissions().size(); i++) {
                            if (file.getName().equals(data.getBlackboardSubmissions().get(i).getName())) {
                                contains = true;
                            }
                        }
                        if (!contains) {
                            data.addBlackBoardSubmissions(zipFile);
                        }
                        contains = false;
                    }
                }
                ObservableList<zipFile> model = data.getBlackboardSubmissions();
                hwTableView.setItems(model);
            });
            removeButton.setOnAction(e -> {
                AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
                yesNoDialog.show("Remove", "Are you sure you want to remove?");
                String selection = yesNoDialog.getSelection();
                if (selection.equals(AppYesNoCancelDialogSingleton.YES)) {
                    zipFile file = hwTableView.getSelectionModel().getSelectedItems().get(0);
                    try {
                        Files.deleteIfExists(file.getPath());
                        data.getBlackboardSubmissions().remove(file);
                    } catch (Exception o) {
                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle("INFORMATION");
                        alert.setHeaderText("Deletion Failure");
                        alert.setContentText("File named " + file.getName() + " cannot be deleted because it is being used by another process, please try again.");
                        alert.showAndWait();
                    }
                    removeButton.setDisable(true);
                    hwTableView.refresh();
                    clearSelection();
                    app.getGUI().getFileController().Saved(false);
                }
            });
            hwTableView.setOnMouseClicked(e -> {
                if (hwTableView.getSelectionModel().getSelectedItems().size() != 0) {
                    removeButton.setDisable(false);
                    actionButton.setDisable(false);
                    if (hwTableView.getSelectionModel().getSelectedItems().size() > 1) {
                        viewButton.setDisable(true);
                    } else {
                        viewButton.setDisable(false);
                    }
                }

            });
            viewButton.setOnAction(e -> {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Directory Contents");
                alert.setHeaderText(null);
                String fileContents = "";
                ZipFile File = null;
                try {
                    File = new ZipFile(hwTableView.getSelectionModel().getSelectedItems().get(0).getFile());
                } catch (Exception o) {
                }
                if (File == null) {
                    viewButton.setDisable(true);
                } else {
                    Enumeration zipEntries = File.entries();
                    while (zipEntries.hasMoreElements()) {
                        fileContents += ((ZipEntry) zipEntries.nextElement()).getName() + "\n";
                    }
                    alert.setContentText(fileContents);
                    alert.getDialogPane().setMinWidth(600);
                    alert.showAndWait();
                }
            });
            if (actionButtonBox.getChildren().size() > 1) {
                actionButtonBox.getChildren().remove(actionButton2);
            }
            if (leftBox.getChildren().size() > 3) {
                leftBox.getChildren().remove(sourceFileTypeLabel);
                leftBox.getChildren().remove(fileTypeGridPane);
            }
        } else if (page == 2) {
            infoTextArea.setText(data.getTextArea2());
            if (hwTableView.getColumns().size() == 1) {
                hwTableView.getColumns().remove(0);
                hwTableView.getColumns().add(studentSubmissionsColumn);
                studentSubmissionsColumn.prefWidthProperty().bind(hwTableView.widthProperty().divide(1));
                studentSubmissionsColumn.setCellValueFactory(new PropertyValueFactory<zipFile, String>("name"));
                ObservableList<zipFile> model = data.getStudentSubmissions();
                hwTableView.setItems(model);
            }

            progressBar.setProgress(0);
            progressIndicator.setProgress(0);
            leftBox.getChildren().remove(stepLabel);
            stepLabel = new Label(props.getProperty(STEP2_TEXT));
            leftBox.getChildren().add(0, stepLabel);
            stepLabel.getStyleClass().add(CLASS_DARK_TEXT);

            leftBox.getChildren().remove(stepInstructionLabel);
            stepInstructionLabel = new Label(props.getProperty(STEP2_INSTRUCTION_TEXT));
            stepInstructionLabel.getStyleClass().add(CLASS_LIGHT_TEXT);
            leftBox.getChildren().add(1, stepInstructionLabel);

            progressBox.getChildren().remove(progressLabel);
            progressLabel = new Label(props.getProperty(RENAME_PROGRESS_TEXT));
            progressBox.getChildren().add(0, progressLabel);
            progressLabel.getStyleClass().add(CLASS_DARK_TEXT);

            actionButtonBox.getChildren().remove(actionButton);
            actionButton = new Button(props.getProperty(RENAME_FILE_BUTTON_TEXT));
            actionButtonBox.getChildren().add(0, actionButton);
            actionButton.getStyleClass().add(CLASS_EDIT_BUTTON);
            actionButton.setDisable(true);
            actionButton.setTooltip(new Tooltip(props.getProperty(RENAME_TOOLTIP)));
            actionButton.setOnAction(e -> {

                File studentSubmissions = new File(".\\work\\" + data.getDirectoryName() + " folder\\submissions");
                for (File file : studentSubmissions.listFiles()) {
                    if (file.getName().length() > 20) {
                        int startIndex = file.getName().indexOf("_", 0) + 1;
                        int endIndex = file.getName().indexOf("_", startIndex);
                        String newName = file.getName().substring(startIndex, endIndex);
                        zipFile zipFile = null;
                        try {
                            zipFile = new zipFile(file);
                        } catch (Exception o) {
                        }
                        if (!zipFile.renameTo(".\\work\\" + data.getDirectoryName() + " folder\\submissions\\" + newName)) {
                            data.setTextArea2F(file.getName() + "\n");
                        } else {
                            data.setTextArea2S(file.getName() + "\nbecomes " + newName + ".zip" + "\n");
                        }

                    } else {
                        data.setTextArea2F(file.getName() + "\n");
                    }
                }

                Task<Void> task = new Task<Void>() {
                    double perc = 0;
                    double max = 200;

                    @Override
                    protected Void call() throws Exception {
                        for (int i = 0; i < 200; i++) {
                            perc = i / max;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    progressIndicator.setProgress(perc);
                                    progressBar.setProgress(perc);
                                }

                            });
                            Thread.sleep(3);
                        }
                        data.setTextArea2(data.getTextArea2S() + "\n" + data.getTextArea2F());
                        infoTextArea.setText(data.getTextArea2());
                        nextButton.setDisable(false);
                        return null;
                    }
                };
                Thread thread = new Thread(task);
                thread.start();

                clearSelection();
                renamed = true;
                app.getGUI().getFileController().Saved(false);
            });
            refreshButton.setOnAction(e -> {
                File studentSubmissions = new File(".\\work\\" + data.getDirectoryName() + " folder\\submissions");
                boolean add = true;
                if (studentSubmissions.list().length > 0) {
                    for (File file : studentSubmissions.listFiles()) {
                        if (file.getName().endsWith(".txt")) {
                            file.delete();
                        } else {
                            zipFile zipFile = null;
                            try {
                                zipFile = new zipFile(file);
                            } catch (Exception o) {
                            }
                            for (int i = 0; i < data.getStudentSubmissions().size(); i++) {

                                if (file.getName().length() < 20) {
                                    add = false;
                                    break;
                                }
                                if (file.getName().equals(data.getStudentSubmissions().get(i).getName())) {
                                    add = false;
                                    break;
                                }
                            }
                            if (add) {
                                data.addStudentSubmissions(zipFile);

                            }
                            add = true;
                        }
                    }
                }
                ObservableList<zipFile> model = data.getStudentSubmissions();
                hwTableView.setItems(model);
                hwTableView.refresh();
                actionButton.setDisable(false);
            });
            removeButton.setOnAction(e -> {
                AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
                yesNoDialog.show("Remove", "Are you sure you want to remove?");
                String selection = yesNoDialog.getSelection();
                if (selection.equals(AppYesNoCancelDialogSingleton.YES)) {
                    zipFile file = hwTableView.getSelectionModel().getSelectedItems().get(0);

                    try {
                        Files.deleteIfExists(file.getPath());
                        data.getStudentSubmissions().remove(file);
                    } catch (Exception o) {
                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle("INFORMATION");
                        alert.setHeaderText("Deletion Failure");
                        alert.setContentText("File named " + file.getName() + " cannot be deleted because it is being used by another process, please try again.");
                        alert.showAndWait();
                    }
                    removeButton.setDisable(true);
                    hwTableView.refresh();
                    clearSelection();
                    app.getGUI().getFileController().Saved(false);
                }
            });
            hwTableView.setOnMouseClicked(e -> {
                if (hwTableView.getSelectionModel().getSelectedItems().size() != 0) {
                    removeButton.setDisable(false);
                    actionButton.setDisable(false);
                    if (hwTableView.getSelectionModel().getSelectedItems().size() > 1) {
                        viewButton.setDisable(true);
                    } else {
                        viewButton.setDisable(false);
                    }
                }

                if (renamed) {
                    viewButton.setDisable(true);
                    removeButton.setDisable(true);
                }

            });
            viewButton.setOnAction(e -> {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Directory Contents");
                alert.setHeaderText(null);
                String fileContents = "";
                ZipFile File = null;
                try {
                    File = new ZipFile(hwTableView.getSelectionModel().getSelectedItems().get(0).getFile());
                } catch (Exception o) {

                }
                if (File == null) {
                    viewButton.setDisable(true);
                } else {
                    Enumeration zipEntries = File.entries();
                    while (zipEntries.hasMoreElements()) {
                        fileContents += ((ZipEntry) zipEntries.nextElement()).getName() + "\n";
                    }
                    alert.setContentText(fileContents);
                    alert.getDialogPane().setMinWidth(600);
                    alert.showAndWait();
                }
            });
            if (actionButtonBox.getChildren().size() > 1) {
                actionButtonBox.getChildren().remove(actionButton2);
            }
            if (leftBox.getChildren().size() > 3) {
                leftBox.getChildren().remove(sourceFileTypeLabel);
                leftBox.getChildren().remove(fileTypeGridPane);
            }
        } else if (page == 3) {
            infoTextArea.setText(data.getTextArea3());
            if (hwTableView.getColumns().size() == 1) {
                hwTableView.getColumns().remove(0);
                hwTableView.getColumns().add(studentZIPFilesColumn);
                studentZIPFilesColumn.prefWidthProperty().bind(hwTableView.widthProperty().divide(1));
                studentZIPFilesColumn.setCellValueFactory(new PropertyValueFactory<zipFile, String>("name"));
                ObservableList<zipFile> model = data.getStudentZIPFiles();
                hwTableView.setItems(model);
            }

            progressBar.setProgress(0);
            progressIndicator.setProgress(0);
            leftBox.getChildren().remove(stepLabel);
            stepLabel = new Label(props.getProperty(STEP3_TEXT));
            leftBox.getChildren().add(0, stepLabel);
            stepLabel.getStyleClass().add(CLASS_DARK_TEXT);

            leftBox.getChildren().remove(stepInstructionLabel);
            stepInstructionLabel = new Label(props.getProperty(STEP3_INSTRUCTION_TEXT));
            stepInstructionLabel.getStyleClass().add(CLASS_LIGHT_TEXT);
            leftBox.getChildren().add(1, stepInstructionLabel);

            progressBox.getChildren().remove(progressLabel);
            progressLabel = new Label(props.getProperty(UNZIP_PROGRESS_TEXT));
            progressBox.getChildren().add(0, progressLabel);
            progressLabel.getStyleClass().add(CLASS_DARK_TEXT);

            actionButtonBox.getChildren().remove(actionButton);
            actionButton = new Button(props.getProperty(UNZIP_BUTTON_TEXT));
            actionButtonBox.getChildren().add(0, actionButton);
            actionButton.getStyleClass().add(CLASS_EDIT_BUTTON);
            actionButton.setTooltip(new Tooltip(props.getProperty(UNZIP_TOOLTIP)));
            actionButton.setDisable(true);
            actionButton.setOnAction(e -> {

                ObservableList<zipFile> file = hwTableView.getSelectionModel().getSelectedItems();
                for (zipFile files : file) {
                    if (files.getName().endsWith(".zip")) {
                        String name = files.getName().replace(".zip", "");
                        new File(".\\work\\" + data.getDirectoryName() + " folder\\projects\\" + name).mkdir();
                        files.extractFile(".\\work\\" + data.getDirectoryName() + " folder\\projects\\" + name);
                        data.setTextArea3S(name + "\n");
                    } else {
                        data.setTextArea3F(files.getName() + "\n");
                    }
                }

                Task<Void> task = new Task<Void>() {
                    double perc = 0;
                    double max = 200;

                    @Override
                    protected Void call() throws Exception {
                        for (int i = 0; i < 200; i++) {
                            perc = i / max;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    progressIndicator.setProgress(perc);
                                    progressBar.setProgress(perc);
                                }

                            });
                            Thread.sleep(6);
                        }
                        data.setTextArea3(data.getTextArea3S() + "\n" + data.getTextArea3F());
                        infoTextArea.setText(data.getTextArea3());
                        return null;
                    }
                };
                Thread thread = new Thread(task);
                thread.start();

                clearSelection();
                File projects = new File(".\\work\\" + data.getDirectoryName() + " folder\\projects\\");
                if (projects.listFiles().length > 0) {
                    nextButton.setDisable(false);
                    app.getGUI().getFileController().Saved(false);
                }
            });
            refreshButton.setOnAction(e -> {
                File studentZIPFiles = new File(".\\work\\" + data.getDirectoryName() + " folder\\submissions");
                boolean add = true;
                if (studentZIPFiles.list().length > 0) {
                    for (File file : studentZIPFiles.listFiles()) {
                        zipFile zipFile = null;
                        try {
                            zipFile = new zipFile(file);
                        } catch (Exception o) {
                        }
                        if (file.getName().length() > 20) {
                            add = false;
                        }
                        for (int i = 0; i < data.getStudentZIPFiles().size(); i++) {
                            if (file.getName().equals(data.getStudentZIPFiles().get(i).getName())) {
                                add = false;
                                break;
                            }
                        }
                        if (add) {
                            data.addstudentZIPFiles(zipFile);
                        }
                        add = true;
                    }
                }
                ObservableList<zipFile> model = data.getStudentZIPFiles();
                hwTableView.setItems(model);
                hwTableView.refresh();
            });
            removeButton.setOnAction(e -> {
                AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
                yesNoDialog.show("Remove", "Are you sure you want to remove?");
                String selection = yesNoDialog.getSelection();
                if (selection.equals(AppYesNoCancelDialogSingleton.YES)) {
                    zipFile file = hwTableView.getSelectionModel().getSelectedItems().get(0);

                    try {
                        Files.deleteIfExists(file.getPath());
                        data.getStudentZIPFiles().remove(file);
                    } catch (Exception o) {
                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle("INFORMATION");
                        alert.setHeaderText("Deletion Failure");
                        alert.setContentText("File named " + file.getName() + " cannot be deleted because it is being used by another process, please try again.");
                        alert.showAndWait();
                    }
                    removeButton.setDisable(true);
                    hwTableView.refresh();
                    clearSelection();
                    app.getGUI().getFileController().Saved(false);
                }
            });
            viewButton.setOnAction(e -> {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Directory Contents");
                alert.setHeaderText(null);
                String fileContents = "";
                ZipFile File = null;
                try {
                    File = new ZipFile(hwTableView.getSelectionModel().getSelectedItems().get(0).getFile());
                } catch (Exception o) {

                }
                if (File == null) {
                    viewButton.setDisable(true);
                } else {
                    Enumeration zipEntries = File.entries();
                    while (zipEntries.hasMoreElements()) {
                        fileContents += ((ZipEntry) zipEntries.nextElement()).getName() + "\n";
                    }
                    alert.setContentText(fileContents);
                    alert.getDialogPane().setMinWidth(600);
                    alert.showAndWait();
                }
            });
            hwTableView.setOnMouseClicked(e -> {
                if (hwTableView.getSelectionModel().getSelectedItems().size() != 0) {
                    removeButton.setDisable(false);
                    actionButton.setDisable(false);
                    if (hwTableView.getSelectionModel().getSelectedItems().size() > 1) {
                        viewButton.setDisable(true);
                    } else {
                        viewButton.setDisable(false);
                    }
                }
            });
            if (actionButtonBox.getChildren().size() > 1) {
                actionButtonBox.getChildren().remove(actionButton2);
            }
            if (leftBox.getChildren().size() > 3) {
                leftBox.getChildren().remove(sourceFileTypeLabel);
                leftBox.getChildren().remove(fileTypeGridPane);
            }
        } else if (page == 4) {
            infoTextArea.setText(data.getTextArea4());
            if (hwTableView.getColumns().size() == 1) {
                hwTableView.getColumns().remove(0);
                studentWorkDirectoriesColumn.setCellValueFactory(new PropertyValueFactory<zipFile, String>("name"));
                hwTableView.getColumns().add(studentWorkDirectoriesColumn);
                studentWorkDirectoriesColumn.prefWidthProperty().bind(hwTableView.widthProperty().divide(1));
                ObservableList<zipFile> model = data.getStudentWorkDirectories();
                hwTableView.setItems(model);
            }

            progressBar.setProgress(0);
            progressIndicator.setProgress(0);
            leftBox.getChildren().remove(stepLabel);
            stepLabel = new Label(props.getProperty(STEP4_TEXT));
            leftBox.getChildren().add(0, stepLabel);
            stepLabel.getStyleClass().add(CLASS_DARK_TEXT);

            leftBox.getChildren().remove(stepInstructionLabel);
            stepInstructionLabel = new Label(props.getProperty(STEP4_INSTRUCTION_TEXT));
            stepInstructionLabel.getStyleClass().add(CLASS_LIGHT_TEXT);
            leftBox.getChildren().add(1, stepInstructionLabel);

            progressBox.getChildren().remove(progressLabel);
            progressLabel = new Label(props.getProperty(CODE_PROGRESS_TEXT));
            progressBox.getChildren().add(0, progressLabel);
            progressLabel.getStyleClass().add(CLASS_DARK_TEXT);

            actionButtonBox.getChildren().remove(actionButton);
            actionButton = new Button(props.getProperty(EXTRACT_CODE_BUTTON_TEXT));
            actionButtonBox.getChildren().add(0, actionButton);
            actionButton.getStyleClass().add(CLASS_EDIT_BUTTON);
            actionButton.setTooltip(new Tooltip(props.getProperty(EXTRACT_CODE_TOOLTIP)));
            actionButton.setDisable(true);

            if (actionButtonBox.getChildren().size() > 1) {
                actionButtonBox.getChildren().remove(actionButton2);
            }
            refreshButton.setOnAction(e -> {
                File studentWorkDirectories = new File(".\\work\\" + data.getDirectoryName() + " folder\\projects");
                boolean add = true;
                if (studentWorkDirectories.list().length > 0) {
                    for (File file : studentWorkDirectories.listFiles()) {
                        zipFile zipFile = null;
                        try {
                            zipFile = new zipFile(file);
                        } catch (Exception o) {
                        }
                        for (int i = 0; i < data.getStudentWorkDirectories().size(); i++) {
                            if (file.getName().equals(data.getStudentWorkDirectories().get(i).getName())) {
                                add = false;
                            }
                        }
                        if (add) {
                            data.addStudentWorkDirectories(zipFile);
                        }
                        add = true;
                    }
                }
                ObservableList<zipFile> model = data.getStudentWorkDirectories();
                hwTableView.setItems(model);
                hwTableView.refresh();
            });
            removeButton.setOnAction(e -> {
                AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
                yesNoDialog.show("Remove", "Are you sure you want to remove?");
                String selection = yesNoDialog.getSelection();
                if (selection.equals(AppYesNoCancelDialogSingleton.YES)) {
                    zipFile file = hwTableView.getSelectionModel().getSelectedItems().get(0);
                    try {
                        FileUtils.deleteDirectory(new File(file.getPath().toString()));
                        data.getStudentWorkDirectories().remove(file);
                    } catch (Exception o) {
                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle("INFORMATION");
                        alert.setHeaderText("Deletion Failure");
                        alert.setContentText("File named " + file.getName() + " cannot be deleted because it is being used by another process, please try again.");
                        alert.showAndWait();
                    }
                    removeButton.setDisable(true);
                    hwTableView.refresh();
                    clearSelection();
                    app.getGUI().getFileController().Saved(false);
                }
            });
            actionButton.setOnAction(e -> {

                for (int i = 0; i < data.getStudentWorkDirectories().size(); i++) {
                    String studentName = data.getStudentWorkDirectories().get(i).getName();
                    File file = new File(".\\work\\" + data.getDirectoryName() + " folder\\projects\\" + studentName);
                    new File(".\\work\\" + data.getDirectoryName() + " folder\\code\\" + studentName).mkdir();
                    data.setTextArea4S("-" + studentName + "\n");
                    String regex = "";
                    if (javaCheckBox.isSelected()) {
                        regex += "(^.+\\.java$)";
                    }
                    if (jsCheckBox.isSelected()) {
                        if (regex == "") {
                            regex += "(^.+\\.js$)";
                        } else {
                            regex += "|(^.+\\.js$)";
                        }
                    }
                    if (jppCheckBox.isSelected()) {
                        if (regex == "") {
                            regex += "(^.+\\.c$)|(^.+\\.h$)|(^.+\\.cpp$)";
                        } else {
                            regex += "|(^.+\\.c$)|(^.+\\.h$)|(^.+\\.cpp$)";
                        }
                    }
                    if (csCheckBox.isSelected()) {
                        if (regex == "") {
                            regex += "(^.+\\.cs$)";
                        } else {
                            regex += "|(^.+\\.cs$)";
                        }
                    }
                    if (promptCheckBox.isSelected()) {
                        if (regex == "") {
                            regex += "(^.+\\" + fileTypeTextField.getText() + "$)";
                        } else {
                            regex += "|(^.+\\" + fileTypeTextField.getText() + "$)";
                        }
                    }
                    Collection<File> files = FileUtils.listFiles(file, new RegexFileFilter(regex), DirectoryFileFilter.DIRECTORY);
                    for (File sc : files) {
                        File oldFile = sc;
                        File newFile = new File(".\\work\\" + data.getDirectoryName() + " folder\\code\\" + studentName + "\\" + sc.getName());
                        try {
                            FileUtils.copyFile(oldFile, newFile);
                        } catch (Exception o) {
                            o.printStackTrace();
                        }
                        data.setTextArea4S("---" + sc.getName() + "\n");
                    }
                }

                Task<Void> task = new Task<Void>() {
                    double perc = 0;
                    double max = 200;

                    @Override
                    protected Void call() throws Exception {
                        for (int i = 0; i < 200; i++) {
                            perc = i / max;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    progressIndicator.setProgress(perc);
                                    progressBar.setProgress(perc);
                                }

                            });
                            Thread.sleep(5);
                        }
                        data.setTextArea4(data.getTextArea4S() + "\n" + data.getTextArea4F());
                        infoTextArea.setText(data.getTextArea4());
                        nextButton.setDisable(false);
                        return null;
                    }
                };
                Thread thread = new Thread(task);
                thread.start();
                actionButton.setDisable(true);
                
                app.getGUI().getFileController().Saved(false);
            });
            viewButton.setOnAction(e -> {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Directory Contents");
                alert.setHeaderText(null);
                String fileContents = "";
                File file = new File(hwTableView.getSelectionModel().getSelectedItems().get(0).getFile().getPath());
                Collection<File> files = FileUtils.listFiles(file, new RegexFileFilter("^(.*?)"), DirectoryFileFilter.DIRECTORY);
                for (File Files : files) {
                    fileContents += Files.getName() + "\n";
                }
                alert.setContentText(fileContents);
                alert.getDialogPane().setMinWidth(600);
                alert.showAndWait();
            });
            hwTableView.setOnMouseClicked(e -> {
                if (hwTableView.getSelectionModel().getSelectedItems().size() != 0) {
                    removeButton.setDisable(false);
                    if (hwTableView.getSelectionModel().getSelectedItems().size() > 1) {
                        viewButton.setDisable(true);
                    } else {
                        viewButton.setDisable(false);
                    }
                }
            });
            javaCheckBox.setOnAction(e -> {
                if (javaCheckBox.isSelected()) {
                    if (data.getStudentWorkDirectories().size() > 0) {
                        actionButton.setDisable(false);
                    }
                } else {
                    if (isClear()) {
                        actionButton.setDisable(true);
                    }
                }
            });
            jsCheckBox.setOnAction(e -> {
                if (jsCheckBox.isSelected()) {
                    if (data.getStudentWorkDirectories().size() > 0) {
                        actionButton.setDisable(false);
                    }
                } else {
                    if (isClear()) {
                        actionButton.setDisable(true);
                    }
                }
            });
            jppCheckBox.setOnAction(e -> {
                if (jppCheckBox.isSelected()) {
                    if (data.getStudentWorkDirectories().size() > 0) {
                        actionButton.setDisable(false);
                    }
                } else {
                    if (isClear()) {
                        actionButton.setDisable(true);
                    }
                }
            });
            csCheckBox.setOnAction(e -> {
                if (csCheckBox.isSelected()) {
                    if (data.getStudentWorkDirectories().size() > 0) {
                        actionButton.setDisable(false);
                    }
                } else {
                    if (isClear()) {
                        actionButton.setDisable(true);
                    }
                }
            });
            promptCheckBox.setOnAction(e -> {
                if (promptCheckBox.isSelected()) {
                    fileTypeTextField.setDisable(false);
                    if (data.getStudentWorkDirectories().size() > 0 && !fileTypeTextField.getText().isEmpty() && fileTypeTextField.getText().startsWith(".")) {
                        actionButton.setDisable(false);
                    }else
                        actionButton.setDisable(true);
                } else {
                    fileTypeTextField.setDisable(true);
                    if (isClear()) {
                        actionButton.setDisable(true);
                    }
                }
            });
            fileTypeTextField.setOnKeyPressed(e -> {
                if (!fileTypeTextField.getText().startsWith(".")) {
                    actionButton.setDisable(true);
                } else {
                    actionButton.setDisable(false);
                }
            });

            leftBox.getChildren().add(sourceFileTypeLabel);
            sourceFileTypeLabel.getStyleClass().add(CLASS_DARK_TEXT);
            fileTypeGridPane = new GridPane();
            leftBox.getChildren().add(fileTypeGridPane);
            fileTypeGridPane.add(javaCheckBox, 0, 0);
            fileTypeGridPane.add(jsCheckBox, 1, 0);
            fileTypeGridPane.add(jppCheckBox, 0, 1);
            fileTypeGridPane.add(csCheckBox, 1, 1);
            fileTypeGridPane.add(fileTypeBox, 0, 2);
            fileTypeGridPane.setHgap(100);
            fileTypeGridPane.setVgap(10);
            javaCheckBox.getStyleClass().add(CLASS_CHECK_BOX);
            jsCheckBox.getStyleClass().add(CLASS_CHECK_BOX);
            jppCheckBox.getStyleClass().add(CLASS_CHECK_BOX);
            csCheckBox.getStyleClass().add(CLASS_CHECK_BOX);
            promptCheckBox.getStyleClass().add(CLASS_CHECK_BOX);
            fileTypeTextField.getStyleClass().add(FILE_TYPE_TEXTFIELD);
        } else {
            infoTextArea.setText(data.getTextArea5a());
            if (hwTableView.getColumns().size() == 1) {
                hwTableView.getColumns().remove(0);
                studentWorkColumn.setCellValueFactory(new PropertyValueFactory<zipFile, String>("name"));
                hwTableView.getColumns().add(studentWorkColumn);
                studentWorkColumn.prefWidthProperty().bind(hwTableView.widthProperty().divide(1));
                ObservableList<zipFile> model = data.getStudentWork();
                hwTableView.setItems(model);
            }

            progressBar.setProgress(0);
            progressIndicator.setProgress(0);
            leftBox.getChildren().remove(stepLabel);
            stepLabel = new Label(props.getProperty(STEP5_TEXT));
            leftBox.getChildren().add(0, stepLabel);
            stepLabel.getStyleClass().add(CLASS_DARK_TEXT);

            leftBox.getChildren().remove(stepInstructionLabel);
            stepInstructionLabel = new Label(props.getProperty(STEP5_INSTRUCTION_TEXT));
            stepInstructionLabel.getStyleClass().add(CLASS_LIGHT_TEXT);
            leftBox.getChildren().add(1, stepInstructionLabel);

            progressBox.getChildren().remove(progressLabel);
            progressLabel = new Label(props.getProperty(CHECK_PROGRESS_TEXT));
            progressBox.getChildren().add(0, progressLabel);
            progressLabel.getStyleClass().add(CLASS_DARK_TEXT);

            actionButtonBox.getChildren().remove(actionButton);
            actionButton = new Button(props.getProperty(CODE_CHECK_BUTTON_TEXT));
            actionButton2 = new Button(props.getProperty(VIEW_RESULTS_BUTTON_TEXT));
            actionButtonBox.getChildren().add(0, actionButton);
            actionButtonBox.getChildren().add(1, actionButton2);
            actionButton.getStyleClass().add(CLASS_EDIT_BUTTON);
            actionButton2.getStyleClass().add(CLASS_EDIT_BUTTON);
            actionButtonBox.getStyleClass().add(CLASS_SPACING);
            actionButton.setTooltip(new Tooltip(props.getProperty(CODE_CHECK_TOOLTIP)));
            actionButton2.setTooltip(new Tooltip(props.getProperty(VIEW_RESULTS_TOOLTIP)));
            actionButton.setDisable(true);
            actionButton2.setDisable(true);
            refreshButton.setOnAction(e -> {
                File studentWork = new File(".\\work\\" + data.getDirectoryName() + " folder\\code");
                boolean add = true;
                if (studentWork.list().length > 0) {
                    for (File file : studentWork.listFiles()) {
                        zipFile zipFile = null;
                        try {
                            zipFile = new zipFile(file);
                        } catch (Exception o) {
                        }
                        for (int i = 0; i < data.getStudentWork().size(); i++) {
                            if (file.getName().equals(data.getStudentWork().get(i).getName())) {
                                add = false;
                            }
                        }
                        if (add) {
                            data.addStudentWork(zipFile);
                        }
                        add = true;
                    }
                }
                ObservableList<zipFile> model = data.getStudentWork();
                hwTableView.setItems(model);
                hwTableView.refresh();
                if (data.getStudentWork().size() > 0) {
                    actionButton.setDisable(false);
                }
            });
            removeButton.setOnAction(e -> {
                AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
                yesNoDialog.show("Remove", "Are you sure you want to remove?");
                String selection = yesNoDialog.getSelection();
                if (selection.equals(AppYesNoCancelDialogSingleton.YES)) {
                    zipFile file = hwTableView.getSelectionModel().getSelectedItems().get(0);
                    try {
                        FileUtils.deleteDirectory(new File(file.getPath().toString()));
                        data.getStudentWork().remove(file);
                    } catch (Exception o) {
                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle("INFORMATION");
                        alert.setHeaderText("Deletion Failure");
                        alert.setContentText("File named " + file.getName() + " cannot be deleted because it is being used by another process, please try again.");
                        alert.showAndWait();
                    }
                    removeButton.setDisable(true);
                    hwTableView.refresh();
                    clearSelection();
                    app.getGUI().getFileController().Saved(false);
                }
            });
            actionButton.setOnAction(e -> {
                Task<Void> task = new Task<Void>() {
                    double max = 200;
                    double perc;

                    @Override
                    protected Void call() throws Exception {
                        for (int i = 0; i < 200; i++) {
                            perc = i / max;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    progressIndicator.setProgress(perc);
                                    progressBar.setProgress(perc);
                                }
                            });
                            Thread.sleep(5);
                        }
                        data.setTextArea5("http://www.google.com");
                        data.setTextArea5a(data.getTextArea5());
                        infoTextArea.setText(data.getTextArea5a());
                        actionButton2.setDisable(false);
                        return null;
                    }
                };
                // THIS GETS THE THREAD ROLLING
                Thread thread = new Thread(task);
                thread.start();

                actionButton.setDisable(true);
                app.getGUI().getFileController().Saved(false);
            });
            actionButton2.setOnAction(e -> {
                WebView webView = new WebView();
                WebEngine engine = webView.getEngine();
                engine.load("https://www.google.com");
                Stage webStage = new Stage();
                VBox root = new VBox();
                root.getChildren().addAll(webView);
                Scene scene = new Scene(root, 800, 600);
                webStage.setScene(scene);
                webStage.initModality(Modality.APPLICATION_MODAL);
                webStage.showAndWait();
                app.getGUI().getFileController().Saved(false);
            });
            if (leftBox.getChildren().size() > 3) {
                leftBox.getChildren().remove(sourceFileTypeLabel);
                leftBox.getChildren().remove(fileTypeGridPane);
            }
        }
    }

    @Override
    public void resetWorkspace() {
        infoTextArea.clear();
        homeButton.setDisable(true);
        previousButton.setDisable(true);
        removeButton.setDisable(true);
        refreshButton.setDisable(true);
        viewButton.setDisable(true);
        actionButton.setDisable(true);
        nextButton.setDisable(true);
        page = 1;
        updatePage();
        if (app.getGUI().getWindow().getTitle() == null) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            app.getGUI().getWindow().setTitle(props.getProperty(APP_TITLE) + " - " + data.getDirectoryName());
        }
        javaCheckBox.setSelected(false);
        jsCheckBox.setSelected(false);
        jppCheckBox.setSelected(false);
        csCheckBox.setSelected(false);
        promptCheckBox.setSelected(false);
        //clear table
        progressBar.setProgress(0);
        progressIndicator.setProgress(0);
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {

    }

    public void handleNewRequest() {

        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        // RESET THE WORKSPACE
        app.getGUI().getFileController().Saved(false);
        app.getGUI().getFileController().setCurrentWorkFile(null);
        resetWorkspace();
        data.resetData();
        updateWorkSpaceButton();
        // MAKE SURE THE WORKSPACE IS ACTIVATED
        activateWorkspace(app.getGUI().getAppPane());
        // TELL THE USER NEW WORK IS UNDERWAY
        dialog.show(props.getProperty(NEW_COMPLETED_TITLE), props.getProperty(NEW_COMPLETED_MESSAGE));
        app.getGUI().setRenameButton(false);
    }

    public void clearSelection() {
        hwTableView.getSelectionModel().clearSelection();
        removeButton.setDisable(true);
        viewButton.setDisable(true);
        actionButton.setDisable(true);
    }

    public boolean isClear() {
        return !javaCheckBox.isSelected() && !jsCheckBox.isSelected()
                && !jppCheckBox.isSelected() && !csCheckBox.isSelected() && !promptCheckBox.isSelected();
    }

    public void clearTextArea() {
        infoTextArea.clear();
        data.resetData();
    }

    public void setTextArea() {
        infoTextArea.setText(data.getTextArea1());
    }

    public void updateTable() {
        blackboardSubmissionsColumn.setCellValueFactory(new PropertyValueFactory<zipFile, String>("name"));
        ObservableList<zipFile> model = data.getBlackboardSubmissions();
        hwTableView.setItems(model);
    }
}
