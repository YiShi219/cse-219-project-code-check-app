package sc.workspace;

import static djf.settings.AppPropertyType.APP_CSS;
import static djf.settings.AppPropertyType.APP_LOGO;
import static djf.settings.AppPropertyType.APP_PATH_CSS;
import static djf.settings.AppPropertyType.APP_TITLE;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import static djf.settings.AppStartupConstants.PATH_WORK;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import sc.CodeCheckApp;
import static sc.CodeCheckProp.CODE_CHECK_IMAGE;
import static sc.CodeCheckProp.CREATE_NEW_CODECHECK_TEXT;
import static sc.CodeCheckProp.RECENT_WORK_TEXT;
import static sc.CodeCheckProp.WELCOME_TEXT;
import sc.data.CodeCheckData;
import static sc.style.CodeCheckStyle.CLASS_DARK_TEXT;
import static sc.style.CodeCheckStyle.WELCOME_PAGE;
import static sc.style.CodeCheckStyle.WELCOME_PAGE1;

/**
 *
 * @author Yi Shi
 */
public class CodeCheckWelcomePage {

    Stage welcomeStage;
    Scene welcomeScene;
    BorderPane welcomePane;
    VBox recentWorkBox;
    VBox codeCheckBox;
    Label recentWorkLabel;
    Hyperlink createNewCodeCheckLink;
    PropertiesManager props = PropertiesManager.getPropertiesManager();
    CodeCheckApp app;

    public CodeCheckWelcomePage(CodeCheckApp initApp) {
        app = initApp;
        initWelcomePage();

    }

    public void initWelcomePage() {
        for (String x : CodeCheckData.getWork()) {
            if (x == null) {
                CodeCheckData.removeWork(x);
            }
        }
        try {
            app.getFileComponent().loadRecentWork();
        } catch (IOException ioe) {
        }
        //CodeCheckData.clearRecentWork();
        welcomeStage = new Stage();
        recentWorkBox = new VBox();
        codeCheckBox = new VBox();
        recentWorkLabel = new Label(props.getProperty(RECENT_WORK_TEXT));
        createNewCodeCheckLink = new Hyperlink(props.getProperty(CREATE_NEW_CODECHECK_TEXT));
        String codeCheckImage = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(CODE_CHECK_IMAGE);
        codeCheckBox.getChildren().add(new ImageView(new Image(codeCheckImage)));
        codeCheckBox.getChildren().add(createNewCodeCheckLink);
        createNewCodeCheckLink.setTranslateX(200);
        createNewCodeCheckLink.setTranslateY(300);
        recentWorkBox.getChildren().add(recentWorkLabel);
        CodeCheckData data = new CodeCheckData(app);

        for (int i = CodeCheckData.getWork().size() - 1; i >= 0; i--) {
            Hyperlink workLink = new Hyperlink(CodeCheckData.getRecentWork(i));
            recentWorkBox.getChildren().add(workLink);
            workLink.setOnAction(e -> {
                try {
                    File file = new File(".\\work\\" + workLink.getText());
                    app.getFileComponent().loadData(app.getDataComponent(), ".\\work\\" + workLink.getText());
                    app.getWorkspaceComponent().setTextArea();
                    app.getWorkspaceComponent().activateWorkspace(app.getGUI().getAppPane());
                    app.getGUI().getFileController().Saved(true);
                    app.getGUI().updateToolbarControls(false);
                    app.getWorkspaceComponent().updateWorkSpaceButton();
                    app.getGUI().getWindow().setTitle(props.getProperty(APP_TITLE) + " - " + app.getDataComponent().getDirectoryName());
                    app.getGUI().getFileController().setCurrentWorkFile(file);
                    app.getWorkspaceComponent().updateTable();
                } catch (IOException iox) {
                }
                welcomeStage.close();
            });
        }

        welcomeStage.setTitle(props.getProperty(WELCOME_TEXT));
        String appIcon = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(APP_LOGO);
        welcomeStage.getIcons().add(new Image(appIcon));
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        welcomeStage.setX(bounds.getMinX());
        welcomeStage.setY(bounds.getMinY());
        welcomeStage.setWidth(1000);
        welcomeStage.setHeight(700);
        welcomePane = new BorderPane();
        welcomePane.setCenter(recentWorkBox);
        welcomePane.setRight(codeCheckBox);
        welcomeScene = new Scene(welcomePane);
        welcomeStage.setScene(welcomeScene);

        createNewCodeCheckLink.setOnAction(e -> {

            TextInputDialog dialog = new TextInputDialog();
            dialog.setTitle("Name");
            dialog.setHeaderText("Name your code check");
            dialog.setContentText("Please enter a valid name(a-z,A-Z,0-9,special characters:space,underscore): ");
            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()) {
                while (CodeCheckData.getWork().contains(result.get()) || result.get().length() == 0 || !result.get().matches("[a-zA-Z0-9_][a-zA-Z0-9 _]*")) {
                    dialog.setContentText("Invalid name, please try again(a-z,A-Z,0-9,special characters:space,underscore): ");
                    result = dialog.showAndWait();
                    if (!result.isPresent()) {
                        break;
                    }
                }
                if (result.isPresent()) {
                    data.setDirectoryName(result.get());
                    CodeCheckData.addRecentWork(result.get());
                    new File(PATH_WORK + result.get() + " folder").mkdir();
                    new File(PATH_WORK + result.get() + " folder\\blackboard").mkdir();
                    new File(PATH_WORK + result.get() + " folder\\submissions").mkdir();
                    new File(PATH_WORK + result.get() + " folder\\projects").mkdir();
                    new File(PATH_WORK + result.get() + " folder\\code").mkdir();

                }

            }

            if (data.getDirectoryName() != null) {
                app.getWorkspaceComponent().handleNewRequest();
                welcomeStage.close();

            }
        });
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String stylesheet = props.getProperty(APP_PATH_CSS);
        stylesheet += props.getProperty(APP_CSS);
        Class appClass = app.getClass();
        URL stylesheetURL = appClass.getResource(stylesheet);
        String stylesheetPath = stylesheetURL.toExternalForm();
        welcomeScene.getStylesheets().add(stylesheetPath);
        recentWorkLabel.getStyleClass().add(CLASS_DARK_TEXT);
        recentWorkBox.getStyleClass().add(WELCOME_PAGE1);
        codeCheckBox.getStyleClass().add(WELCOME_PAGE);

        welcomeStage.showAndWait();
    }

}
